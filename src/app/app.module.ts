import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RegionalesComponent } from './modules/regionales/regionales.component';
import { SistemasComponent } from './modules/sistemas/sistemas.component';

@NgModule({
  declarations: [
    AppComponent,
    RegionalesComponent,
    SistemasComponent,
  ],
  imports: [BrowserModule, AppRoutingModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
