import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminComponent } from './structure/admin.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ProductAddComponent } from '../products/productAdd/productAdd.component';
import { ProductListComponent } from '../products/productList/productList.component';
import { RegionalesComponent } from '../regionales/regionales.component';
import { SistemasComponent } from '../sistemas/sistemas.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        component: AdminComponent,
        children: [
          { path: '', component: DashboardComponent },
          { path: 'add', component: ProductAddComponent },
          { path: 'list', component: ProductListComponent },
          { path: 'regionales', component: RegionalesComponent },
          { path: 'sistemas', component: SistemasComponent },
        ],
      },
      {
        path: '**',
        component: AdminComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class StructureRoutingModule {}
