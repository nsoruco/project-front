import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: 'login.component.html',
  styleUrls: [],
})
export class LoginComponent {
  constructor(private router: Router) {}

  LogIn() {
    this.router.navigate(['/admin']);
  }
}
